#!/usr/bin/env bash
#
#----------[ Script to Validate Regular Expressions ]------------+
#                                                                |
#  script : regex.sh                                             |
#  author : Marcos Oliveira <binbash@linuxmail.org>              |
#  site   : http://terminalroot.com.br                           |
#  version: 1.0.2                                                |
#  date   : dom 29 jul 2018 17:58:31 -03                         |
#  usage  : ./regex.sh -h                                        |
#                                                                |
#----------------------------------------------------------------+
#

shopt -s extglob

usage() {
  cat <<EOF

usage: ${0##*/} [flag] regexp

  Options:
    -i IP       Validate IPv4 address
    -p PHONE    Validate Telephone Numbers in the format ZZ-XXXXX-YYYY*
    -d DATE     Validates dates in XX/XX/XXXX format
    -e MAIL     Include pseudofs mounts
    -h          Displays this help content
    -v          Report the version of this program

  Examples:
    IP:    ./regex.sh -i 192.168.1.100
    IP:    ./regex.sh -i 0.0.0.0
    PHONE: ./regex.sh -p 11-97482-1245
    PHONE: ./regex.sh -p 11-9748-1245
    PHONE: ./regex.sh -p 1197481245
    PHONE: ./regex.sh -p 119748-1245
    PHONE: ./regex.sh -p 11-97481245

EOF
}

if [[ $1 = @(-h|--help) ]]; then
  usage
  exit $(( $# ? 0 : 1 ))
fi

# Don't use quotation (""/'') marks in expressions
ipv4(){
	[[ "$*" =~ ^[0-9]{1,3}(\.[0-9]{1,3}){1,3}$ ]] && echo -e "\e[32;1m✔ Yes\e[m" || echo -e "\e[31;1m✖ No\e[m"
}

phone(){
	[[ "$*" =~ ^[1-9]{2}\-?[0-9]{4,5}\-?[0-9]{4}$ ]] && echo -e "\e[32;1m✔ Yes\e[m" || echo -e "\e[31;1m✖ No\e[m"
}

date_format(){
	[[ "$*" =~ ^([0-9]{2}\/){2}[0-9]{4}$ ]] && echo -e "\e[32;1m✔ Yes\e[m" || echo -e "\e[31;1m✖ No\e[m"
}

_mail(){
	[[ "$*" =~ ^[a-z0-9.]+@[a-z0-9]+\.[a-z]+(\.[a-z]+)?$ ]] && echo -e "\e[32;1m✔ Yes\e[m" || echo -e "\e[31;1m✖ No\e[m"
}

while getopts ':i:p:d:e:hv' flag; do
  case $flag in
    i) ipv4 "$OPTARG";;
    p) phone "$OPTARG";;
    d) date_format "$OPTARG";;
    e) _mail "$OPTARG";;
    v) sed -n '/^#.*version/p' ${0##*/} | sed 's/^#.*version..//';;
    ?) usage ;;
  esac
done
