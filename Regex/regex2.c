#include <stdio.h>
#include <stdlib.h>
#include <regex.h>

int main(int argc, char **argv){

	int start, error;

	regex_t er;
	regmatch_t match;

	regcomp(&er, argv[1], REG_EXTENDED);
	start = 0;
	error = regexec(&er, argv[2], 1, &match, 0);

	while (error == 0){
		printf("Texto de pesquisa: %s\n", argv[2]+start);
		printf("Casou de %d a %d\n", match.rm_so, match.rm_eo);
		start += match.rm_eo;
		error = regexec(&er, argv[2]+start, 1, &match, REG_NOTBOL);
	}

	exit(0);

}
