DROP DATABASE IF EXISTS regex;

CREATE DATABASE regex;

USE regex;

CREATE TABLE dados(id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY, nome TEXT NOT NULL, endereco TEXT NOT NULL, rg TEXT NOT NULL, cpf TEXT NOT NULL, ip TEXT NOT NULL, package TEXT NOT NULL, email TEXT NOT NULL, data TEXT NOT NULL, tel TEXT NOT NULL, url TEXT NOT NULL, frase TEXT NOT NULL);

INSERT INTO dados VALUES (NULL, 'Linus Torvalds', 'Rua do Kernel, 01, Jardim Panic', '265879267', '419.888.573-76', '217.182.168.178', 'apt-get', 'linus@kernel.org', '28/12/1969', '+55 (11) 3458-8974', 'http://terminalroot.com.br/2017/07/como-compilar-o-kernel-do-zero-no-gentoo-linux.html', 'Fuck NVIDIA');

INSERT INTO dados VALUES (NULL, 'Bill Gates', 'Rua das Janelas, 999, Jardim Tela Azul', '895472315', '525.875.321-24', '199.249.223.71', 'cmd.exe', 'gates@microsoft.net', '28/10/1955', '+55 (41) 4587-0215', 'http://terminalroot.com.br/2016/05/diga-adeus-ao-microoft-windows.html', 'Foi só um probleminha');

INSERT INTO dados VALUES (NULL, 'Steve Jobs', 'Rua da Maçã, 5489, Jardim da Cópia', '520236847', '865.919.331-82', '192.42.116.26', 'brew', 'jobs@apple.co.uk', '24/11/1955', '+55 (71) 2410-3691', 'http://terminalroot.com.br/2018/03/como-instalar-o-mac-os-x-em-virtualbox-no-linux.html', 'Nada se Cria, Tudo se copia');

INSERT INTO dados VALUES (NULL, 'Dennis Ritchie', 'Rua do Único, 0215, Jardim do C', '0654852136', '622.114.688-70', '198.50.200.129', 'etc-update', 'c@unix.sh', '09/09/1941', '+55 (85) 3377-2200', 'http://terminalroot.com.br/2016/11/blog-linux-unix.html', 'Eu Preciso C');
