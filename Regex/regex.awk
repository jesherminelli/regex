#!/usr/bin/awk -f
# Script Awk para validar Expressões Regulares
# Uso:
#	chmod +x regex.awk
#	./regex.awk apt-get # apt-get : Casou
#	./regex.awk etc-update # etc-update : Casou
#	./regex.awk semhifen # semhifen : Não casou

BEGIN{
	string = ARGV[1]
	if(string ~ /\w+-\w+/)
		print string ": Casou"
	else
		print string ": Não casou"
}
