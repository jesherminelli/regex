#include <stdio.h>
#include <stdlib.h>
#include <regex.h>

// Compile: gcc regex.c -o regex.o
// ./regex.o
// Uso, ex.: ./regex.o entro dentro
// Saída: Casou
// Se não passar argumentos, dará a mensagem: Falha de segmentação (imagem do núcleo gravada)
// Se a primeira palavra não estiver dentro da segunda a saída será: Não casou
/*
 * regex_t - guarda o padrão de pesquisa no buffer
 * REG_EXTENDED - forma moderna de usar ERs, ou seja, o conteúdo desse curso
 * REG_NOSUB - informa que será somente 1 teste (sim ou não, casou ou não casou)
 */
int main(int argc, char **argv){

	regex_t er;
	regcomp(&er, argv[1], REG_EXTENDED|REG_NOSUB);

	if((regexec(&er, argv[2], 0, NULL, 0)) == 0)
		printf("Casou\n");
	else
		printf("Não casou\n");
	exit(0);

}
