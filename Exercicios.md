## EXPRESSÕES REGULARES

_*Site para praticar*_ - [regex101.com](https://regex101.com/)

#### **Validar IPV4**

- **Crie um arquivo com suas interfaces de rede**
```
ip a 1> interfaces-rede
```

- **Use o comando grep -E ou egrep**
  - _forma longa:_
```
egrep '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' interfaces-rede
```
  - _forma resumida utilizando o agrupamento de comandos **( )**_
```
egrep ''[0-9]{1,3}(\.[0-9]{1,3}){1,3}' interfaces-rede
```

![ipv4](img/validar-ipv4.png)

#### Validar números de telefone, datas de nascimento e email

- **Validar Telefones**
  - _comando_
```
egrep '\(?[1-9]{2}\)?[0-9]{4,5}\-?[0-9]{4}' telefone
```

![telefones](img/tel.png)
---
![regex101.com](img/tel2.png)

- **Validar datas de nascimento**

```
egrep '^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$' datas
```

- _Foma resumida_
```
egrep '^([0-9]{2}\/){2}[0-9]{4}$' datas
```

![datas de nascimento](img/data.png)

- **Validar email**

```
egrep '^[a-z0-9._]+@[a-z0-9]+\.[a-z]{2,3}+(\.[a-z]{2,3})?(\.[a-z]{2})?$' email
```


![email](img/email.png)
--
![regex101.com](img/email2.png)

- **Validar 100% Coverage em testes unitários**

```

sed -n '/[0-9]\{3\}%/p' coverage

egrep '^[0-9]{3}\%$' coverage
```

![](img/coverage.png)

---


- **Validar sites**

```ruby
^www\.[a-zA-Z0-9-]+\.[a-z]{2,}(?:\.[a-z]{2,})?

www.example.com
www.mywebsite.net
www.yourblog.org
www.shoponline.store
www.travelnow.info
www.example.co.uk
www.mywebsite.com.br
www.yourblog.ca
www.shoponline.de
www.travelnow.jp
www.exmpl.com
www.mwb.net
www.yblg.org
www.soln.store
www.tnw.info
www.nave.app.br
www.nave.com.br
www.1rirp.com.br

[21] pry(main)> valid_website_regex = /^www\.[a-zA-Z0-9-]+\.[a-z]{2,}(?:\.[a-z]{2,})?$/
[21] pry(main)> 'www.1rirp.com.br'.match?(valid_website_regex)
=> true
[22] pry(main)> 'https://www.google.com'.match?(valid_website_regex)
=> false


# Another examples

Faker::Internet.domain_name                                                   #=> "altenwerth-gerhold.example"
Faker::Internet.domain_name(subdomain: true)                                  #=> "metz.mclaughlin-brekke.test"
Faker::Internet.domain_name(subdomain: true, domain: 'faker')                 #=> "foo.faker.test"
Faker::Internet.domain_name(domain: 'faker-ruby.org')                         #=> "faker-ruby.org"
Faker::Internet.domain_name(subdomain: true, domain: 'faker-ruby.org')        #=> "foo.faker-ruby.org"
Faker::Internet.domain_name(subdomain: true, domain: 'faker.faker-ruby.org')  #=> "faker.faker-ruby.org"

valid_website_regex = /^www(\.[a-zA-Z0-9-]+)+\.[a-zA-Z]{2,}(?:\.[a-zA-Z]{2,})?$/

5.times do
  website = "www.#{Faker::Internet.domain_name(subdomain: true, domain: 'faker.faker-ruby.org')}"
  puts "#{website} - #{website.match?(valid_website_regex) ? 'Válido' : 'Inválido'}"
end


````